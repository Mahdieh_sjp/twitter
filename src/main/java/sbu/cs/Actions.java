package sbu.cs;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;

import java.sql.Timestamp;
import java.util.*;

public class Actions {

    App app;
    User user;

    Actions(App app){
        this.app = app;
        this.user = app.user;
    }


    public String tweet(String tweet){
        app.tweetsCollection.insertOne(new Document()
                .append("tweet_id", app.tweetsCollection.countDocuments())
                .append("user_id", user.getUsername())
                .append("tweet", tweet)
                .append("date", new Timestamp(System.currentTimeMillis()))
                .append("likes", 0)
                .append("likeList", new ArrayList<String>()));
        return Responses.SUCCESSFUL_TWEET;
    }


    public String follow(String username){

        if(user.getUsername().equals(username)){
            return Responses.SELF_FOLLOW_ERROR;
        }
        Document dc = User.getUserDocument(app, username);
        if (dc == null){
            return Responses.FOLLOW_ERROR;
        }
        else{
            ArrayList<String> followers = User.getUserFollowers(app, username);
            if (followers.contains(user.getUsername())){
                return username + Responses.ALREADY_FOLLOWED;
            }
            else {
                followers.add(user.getUsername());
                app.usersCollection.updateOne(Filters.eq("username", username)
                        , Updates.set("followers", followers));

                ArrayList<String> following = User.getUserFollowing(app, user.getUsername());
                following.add(username);

                app.usersCollection.updateOne(Filters.eq("username", user.getUsername())
                        , Updates.set("following", following));
                return username + Responses.SUCCESSFUL_FOLLOW;
            }
        }
    }


    public String unfollow(String username){

        ArrayList<String> following = User.getUserFollowing(app, user.getUsername());

        if (!following.contains(username)){
            return Responses.UNFOLLOW_ERROR + username;
        }
        else {
            following.remove(username);
            app.usersCollection.updateOne(Filters.eq("username", user.getUsername())
                    , Updates.set("following", following));
            ArrayList<String> followers = User.getUserFollowers(app, user.getUsername());

            followers.remove(user.getUsername());
            app.usersCollection.updateOne(Filters.eq("username", username)
                    , Updates.set("followers", followers));

            return username + Responses.SUCCESSFUL_UNFOLLOW;
        }

    }


    public String profile(String username){
        Document dc = User.getUserDocument(app, username);

        if (dc == null){
            return Responses.PROFILE_ERROR;
        }

        ArrayList<String> timelineList = new ArrayList<>();
        timelineList.add(username);
        printUserInfo(dc
                , User.getUserFollowers(app, username).size()
                , User.getUserFollowing(app, username).size());
        printTweet(app.getTweetsCollection(), timelineList);

        return "";
    }


    public String like(int tweetId){
        Document dc = app.tweetsCollection.find(Filters.eq("tweet_id", tweetId)).first();

        if (dc == null){
            return Responses.LIKE_ERROR;
        }

        ArrayList<String> likeList = (ArrayList<String>) dc.get("likeList");
        if(!likeList.contains(user.getUsername())) {
            likeList.add(user.getUsername());

            app.tweetsCollection.updateOne(Filters.eq("tweet_id", tweetId)
                    , Updates.set("likes"
                            , dc.getInteger("likes") + 1));
            app.tweetsCollection.updateOne(Filters.eq("tweet_id", tweetId)
                    , Updates.set("likeList", likeList));
        }

        return tweetId + Responses.LIKED;
    }


    public static void printUserInfo(Document user, int followers, int following){
        System.out.println("\n" + user.get("username") + "\n********");
        System.out.println("followers: " + followers
                + "\nfollowing: " + following
                + "\n********\n"
                + "joined on " + user.getDate("joined")
                + "\n");
    }


    public static void printTweet(MongoCollection<Document> tweets, ArrayList<String> timelineList){
        Map<Long, String> output = new HashMap<>();

        for(String str: timelineList) {
            Iterable<Document> dc = tweets.find(Filters.eq("user_id", str));
            Iterator<Document> it = dc.iterator();

            while (it.hasNext()) {
                Document tweet = it.next();

                output.put(tweet.getLong("tweet_id"),
                        "\ntweet_id: " + tweet.get("tweet_id")
                                + "\nuser_id: " + tweet.get("user_id")
                                + "\ntweet:\n" + tweet.get("tweet")
                                + "\nposted on " + tweet.getDate("date")
                                + "\n" + tweet.get("likes") + " likes\n"
                                + "\n________________\n");
            }
        }
        output = Actions.sortByKeys(output);
        for (String str : output.values()){
            System.out.println(str);
        }

    }


    public static <K extends Comparable, V> Map<K,V> sortByKeys(Map<K,V> map) {
        Map<K, V> treeMap = new TreeMap<>(new Comparator<K>() {
            @Override
            public int compare(K a, K b) {
                return b.compareTo(a);
            }
        });

        treeMap.putAll(map);

        return treeMap;
    }
}

