package sbu.cs;


import org.bson.Document;


import java.sql.Timestamp;
import java.util.ArrayList;


public class Entry {


    public static String singUp(App app, String username, String password){
        Document dc = User.getUserDocument(app, username);
        if(dc == null){
            app.usersCollection.insertOne(new Document()
                    .append("username", username)
                    .append("password", password)
                    .append("followers", new ArrayList<String>())
                    .append("following", new ArrayList<String>())
                    .append("joined", new Timestamp(System.currentTimeMillis())));
            return Responses.USER_CREATED;
        }
        else{
            return Responses.USERNAME_EXISTS;
        }
    }


    public static String login(App app, String username, String password){
        Document dc = User.getUserDocument(app, username);
        if(dc == null){
            return Responses.USERNAME_DOSE_NOT_EXISTS;
        }
        else {
            if (!dc.get("password").equals(password)){
                return Responses.WRONG_PASSWORD;
            }
            else{ return Responses.LOGGED_IN;}
        }
    }


    public static String logout(User user){
        user.logout();
        return Responses.LOGGED_OUT;
    }


}
