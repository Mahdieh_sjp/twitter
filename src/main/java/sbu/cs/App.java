package sbu.cs;


import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.Scanner;


public class App {

    boolean quit = false;
    boolean entered = false;

    MongoClient client;
    MongoDatabase database;
    MongoCollection<Document> usersCollection;
    MongoCollection<Document> tweetsCollection;


    User user;
    Actions actions;


    App(){
        client = MongoClients.create("mongodb://localhost:27017");
        database = client.getDatabase("twitter");
        usersCollection = database.getCollection("Users");
        tweetsCollection = database.getCollection("Tweets");
    }


    public MongoDatabase getDatabase() {
        return database;
    }


    public MongoCollection<Document> getTweetsCollection() {
        return tweetsCollection;
    }


    public void start(){
        while (!quit){
            if (entered){
                System.out.println("\nAvailable features: My profile, Tweet, Follow, Unfollow, Followers, Following," +
                        "\n Profile, Like, Timeline, Logout, Quit.");;
                Scanner in = new Scanner(System.in);
                String input = in.nextLine().trim().toLowerCase();
                
                if(input.equals("quit")){
                    quit = true;
                }
                
                else if(input.equals("logout")){
                    entered = false;
                    System.out.println(Entry.logout(user));
                }
                
                else if(input.equals("my profile")){
                    user.myProfile();
                }
                
                else if(input.equals("tweet")){
                    String tweet;
                    do {
                        System.out.println("Tweet must be less than 140 characters.");
                        String str = in.nextLine();
                        StringBuilder builder = new StringBuilder();
                        try {
                            while (str.length() != 0) {
                                builder.append(str + "\n");
                                str = in.nextLine();
                            }
                        }catch (Exception e){}
                        tweet = builder.toString();
                    }while (tweet.length() > 140);
                    System.out.println(actions.tweet(tweet));

                }

                else if(input.equals("like")){
                    System.out.println("Enter tweet id: ");
                    int inn = in.nextInt();
                    System.out.println(actions.like(inn));
                }

                else if(input.equals("follow")){
                    System.out.println("Enter username ");
                    String inn = in.next();
                    System.out.println(actions.follow(inn));
                }

                else if(input.equals("unfollow")){
                    System.out.println("Enter username: ");
                    String inn = in.next();
                    System.out.println(actions.unfollow(inn));
                }

                else if(input.equals("profile")){
                    System.out.println("Enter username: ");
                    String inn = in.next();
                    System.out.println(actions.profile(inn));
                }
                
                else if(input.equals("followers")){
                    user.followers();
                }

                else if(input.equals("following")){
                    user.following();
                }

                else if(input.equals("timeline")){
                    user.timeline();
                }

                
            }
            else{
                enter();
            }
        }

        System.out.println("Hope you enjoyed.");
    }

    public void enter(){
        System.out.println("Welcome." +
                "\nAvailable features: Sing up, Login, Quit.");
        Scanner in = new Scanner(System.in);
        String input = in.nextLine().trim().toLowerCase();

        if (input.equals("sign up")){
            System.out.println("Enter username:");
            String username = in.nextLine().trim();
            System.out.println("Enter password:");
            String password = in.nextLine().trim();

            String result = Entry.singUp(this, username, password);

            if (result.equals(Responses.USER_CREATED)){
                entered = true;
                user = new User(this, username, password);
                actions = new Actions(this);
            }

            System.out.println(result);
        }

        else if (input.equals("login")) {
            System.out.println("Enter username:");
            String username = in.nextLine().trim();
            System.out.println("Enter password:");
            String password = in.nextLine().trim();
            String result = Entry.login(this, username, password);

            if (result.equals(Responses.LOGGED_IN)){
                user = new User(this, username, password);
                actions = new Actions(this);
                entered = true;
            }

            System.out.println(result);
        }

        else if (input.equals("quit")){
            quit = true;
        }
    }


    public static void main(String[] args){
        App app = new App();
        app.start();
    }
}
