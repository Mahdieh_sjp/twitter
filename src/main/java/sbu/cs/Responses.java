package sbu.cs;

public class Responses {
    final public static String USER_CREATED = "User successfully created.";
    final public static String USERNAME_EXISTS = "Username already exists.";

    final public static String USERNAME_DOSE_NOT_EXISTS = "Username doesn't exist.";
    final public static String WRONG_PASSWORD = "Wrong password.";

    final public static String LOGGED_IN= "Successfully logged in.";
    final public static String LOGGED_OUT= "Successfully logged out.";

    final public static String SUCCESSFUL_TWEET= "Tweet posted successfully.";

    final public static String FOLLOW_ERROR= "Username you want to follow, doesn't exist.";
    final public static String SUCCESSFUL_FOLLOW = " is in your following list now.";
    final public static String ALREADY_FOLLOWED = " was already followed by you.";
    final public static String SELF_FOLLOW_ERROR = "You can't follow yourself.";

    final public static String UNFOLLOW_ERROR = "You don't follow ";
    final public static String SUCCESSFUL_UNFOLLOW = " unfollowed successfully.";

    final public static String LIKE_ERROR = "Tweet id doesn't exist.";
    final public static String LIKED = " is liked.";

    final public static String PROFILE_ERROR = "User doesn't exist.";








}
