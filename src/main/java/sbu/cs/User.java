package sbu.cs;

import com.mongodb.client.model.Filters;
import org.bson.Document;

import java.util.*;

public class User {

    private String username;
    private String password;

    App app;

    User (App app, String username, String password){
        this.app = app;
        this.username = username;
        this.password = password;
    }


    public String getUsername() {
        return username;
    }


    public void logout(){
        this.username = null;
        this.password = null;
    }


    public void myProfile(){
        Document doc = getUserDocument(app, username);
        ArrayList<String> followers = getUserFollowers(app, username);
        ArrayList<String> following = getUserFollowing(app, username);

        Actions.printUserInfo(doc, followers.size(), following.size());

        ArrayList<String> timelineList = new ArrayList<>();
        timelineList.add(username);

        Actions.printTweet(app.getTweetsCollection(), timelineList);
    }


    public void timeline(){
        ArrayList<String> timelineList = getUserFollowing(app, username);
        timelineList.add(username);

        Actions.printTweet(app.getTweetsCollection(), timelineList);
    }


    public void followers(){
        Document doc = getUserDocument(app, username);
        ArrayList<String> followers = getUserFollowers(app, username);
        ArrayList<String> following = getUserFollowing(app, username);

        Actions.printUserInfo(doc, followers.size(), following.size());

        User.printFollowers(followers);
    }


    public void following(){
        Document doc = getUserDocument(app, username);
        ArrayList<String> followers = getUserFollowers(app, username);
        ArrayList<String> following = getUserFollowing(app, username);

        Actions.printUserInfo(doc, followers.size(), following.size());

        User.printFollowing(following);
    }


    public static Document getUserDocument(App app, String username){
        return app.getDatabase()
                .getCollection("Users")
                .find(Filters.eq("username", username))
                .first();
    }


    public static ArrayList<String> getUserFollowers(App app, String username){
        Document doc = getUserDocument(app, username);
        return (ArrayList<String>)doc.get("followers");
    }


    public static ArrayList<String> getUserFollowing(App app, String username){
        Document doc = getUserDocument(app, username);
        return (ArrayList<String>)doc.get("following");
    }


    public static void printFollowers(ArrayList<String> followers){
        System.out.println("Followers: ");
        for(String str: followers){
            System.out.println(str);
        }
    }


    public static void printFollowing(ArrayList<String> following){
        System.out.println("Following: ");
        for(String str: following){
            System.out.println(str);
        }
    }



}
